<a name="unreleased"></a>
## [Unreleased]


<a name="0.0.2"></a>
## [0.0.2] - 2020-05-12
### Add
- add centos7 test

### Increase
- increase ansible timeout in molecule + add some log by default

### Switch
- switch to last molecule version

### Update
- update git urls...


<a name="0.0.1"></a>
## 0.0.1 - 2019-10-04
### Add
- add a good link in comment
- add some version check
- add fedora
- add README with how to use this repository
- add /var/run/docker.sock mount as we may want to run docker inside docker inside docker inside ...
- add basic travis build conf

### Add
- Add LICENSE

### First
- first commit

### Test
- test with docker:dind

### Try
- try to run all distro test in parallele

### Reverts
- add /var/run/docker.sock mount as we may want to run docker inside docker inside docker inside ...


[Unreleased]: https://gitlab.com/FanchG/ansible-molecule-skeleton/compare/0.0.2...HEAD
[0.0.2]: https://gitlab.com/FanchG/ansible-molecule-skeleton/compare/0.0.1...0.0.2
