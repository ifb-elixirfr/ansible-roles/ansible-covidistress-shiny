COVIDiStress-Shiny
==================

A role to install COVIDiStress-Shiny from https://github.com/COVIDiSTRESS/COVIDiStress-Shiny

Dependencies
------------

[Requirements](molecule/default/requirements.yml)


Variables
---------

[Role Variables](defaults/main.yml)

[Dependencie Variables](vars/main.yml)


Example Playbook
----------------

[Test Playbook](molecule/default/playbook.yml)


License
-------

[License](LICENSE)
